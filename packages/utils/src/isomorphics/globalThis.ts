import { getGlobalThis } from './getGlobalThis';

export const globalThis = getGlobalThis();

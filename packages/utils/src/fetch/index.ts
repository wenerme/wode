export type FetchLike = (url: string | Request, init?: RequestInit) => Promise<Response>;
